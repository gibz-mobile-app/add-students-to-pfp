<?php

// URL des Endpunktes, wohin die Daten geliefert werden sollen.
$url = "https://gibz-app.ch/bffw/api/profile-picture/v1/person";

// Daten, welche an den Server geliefert werden sollen.
// TODO: Musterwerte bitte mit entsprechenden Variablen ersetzen.

// Annahme: Variable, welche die Daten "aller" Lernenden enthält.
$allStudents = [];

// Hinzufügen von Testdaten
// TODO: Diesen Abschnitt für den produktiven Betrieb entfernen.
if (isset($_GET["testdata"]) && $_GET["testdata"] == 1) {
    $allStudents = [
        [
            "gib" => "GIB000011",
            "username" => "hmuster",
            "firstName" => "Hans",
            "lastName" => "Muster",
            "classUniqueName" => "demo2024a",
            "classDisplayName" => "Demo 1a",
        ],
        [
            "gib" => "GIB000012",
            "username" => "vmuster",
            "firstName" => "Verena",
            "lastName" => "Muster",
            "classUniqueName" => "demo2024b",
            "classDisplayName" => "Demo 1b",
        ],
    ];
}

// In dieser Variable werden am Ende der nachfolgenden Schleife die Daten
// aller Lernenden in der geforderten Struktur enthalten sein.
$students = [];

// Die Ursprungsdaten (aus $allStudents) werden in der geforderten Struktur
// zum vorbereiteten Array $students hinzugefügt.
foreach ($allStudents as $student) {
    array_push($students, [
        "gib" => $student["gib"],
        "username" => $student["username"],
        "firstName" => $student["firstName"],
        "lastName" => $student["lastName"],
        "classUniqueName" => $student["classUniqueName"],
        "classDisplayName" => $student["classDisplayName"],
    ]);
}

// Nach der Schleife sollten die Daten gemäss dem nachfolgenden Muster strukturiert sein.
/*
    [
        [
            "gib" => "GIB000011",
            "username" => "hmuster",
            "firstName" => "Hans",
            "lastName" => "Muster",
            "classUniqueName" => "demo2024a",
            "classDisplayName" => "Demo 1a",
        ],
        [
            "gib" => "GIB000012",
            "username" => "vmuster",
            "firstName" => "Verena",
            "lastName" => "Muster",
            "classUniqueName" => "demo2024b",
            "classDisplayName" => "Demo 1b",
        ],
        // more data for other students...
    ]
*/

// Definition der Metadaten des Requests
$options = [
    'http' => [
        'method' => 'POST',
        'header' => "Content-Type: application/json\r\n",
        'content' => json_encode($students),
    ],
];

// Den Request vorbereiten und absenden.
$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);

// Das Resultat verarbeiten.
if ($result === false) {
    /* Handle error */
} else {
    // Der Request bzw. das Anliefern der Daten war erfolgreich.
    // Als Antwort wird vom Server der HTTP-Status 204 (No Content) zurück gegeben.
}
