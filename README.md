# Datenanlieferung für Profilfotos

Für die Erstellung von Profilfotos mit der GIBZ App sollen die Daten von Lernenden des GIBZ bei der Erfassung im System über ein PHP Skript an das Backend der GIBZ App angeliefert werden.

**Das [PHP-Skript](src/index.php) in diesem Repository kann/soll als Ausgangslage für diese Datenanlieferung genutzt werden.**

## Verwendung/Test mit Docker

Das PHP-Skript kann/soll vor dem produktiven Einsatz getestet (bzw. *ausprobiert*) werden. Dafür kann das Image [lokal gebildet](#docker-image-lokal-bilden) oder [aus einer Registry geladen](#docker-image-aus-der-registry-von-gitlab-laden) werden.

### Docker Image lokal bilden

Nach dem Klonen dieses Repositories kann mit dem nachfolgendem Befehl ein Docker Image erstellt werden:

```shell
docker build -t add-students-to-pfp .
```

Danach kann das soeben erstellte Image mit dem nachfolgenden Befehl ausgeführt werden:

```shell
docker run -d --rm -p 8080:80 --name add-students-to-pfp add-students-to-pfp
```

### Docker Image aus der Registry von GitLab laden

Wer das Image nicht lokal bilden möchte, kann stattdessen ein vorbereitetes Image verwenden:

```shell
docker run -d --rm -p 8080:80 --name add-students-to-pfp registry.gitlab.com/gibz-mobile-app/add-students-to-pfp
```

## Lokal ausführen

Sobald der Container gestartet ist, kann im Browser die Url [http://localhost:8080](http://localhost:8080) verwendet werden, um das Skript auszuführen.

Für den Testbetrieb kann der GET-Parameter `testdata` mit dem Wert `1` zur URL hinzugefügt werden: [http://localhost:8080?testdata=1](http://localhost:8080?testdata=1).